import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http"

//import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuComponent } from './components/menu/menu.component';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { DailyLineMonitoringComponent } from './components/daily-line-monitoring/daily-line-monitoring.component';
import { DetailByProcessComponent } from './components/detail-by-process/detail-by-process.component';
import { HistoryComponent } from './components/history/history.component';
import { StockComponent } from './components/stock/stock.component';
import { MainMononitorComponent } from './components/main-mononitor/main-mononitor.component';
import { MasterSetupComponent } from './components/master-setup/master-setup.component';

const appRoutes: Route[] = [
  { path: '', redirectTo: '/main', pathMatch: 'full'},
  { path: 'main', component: MainComponent},
  { path: 'linemonitoring/daily', component: DailyLineMonitoringComponent },
  { path: 'linemonitoring/detail', component: DetailByProcessComponent },
  { path: 'linemonitoring/history', component: HistoryComponent },
  { path: 'linemonitoring/stock', component: StockComponent },
  { path: 'mainmonitor', component: MainMononitorComponent },
  { path: 'setup/master', component: MasterSetupComponent },
  { path: '**', redirectTo: '/main' },
  //{ path: 'login', component: LoginComponent },
  // { path: 'stock', component: StockComponent },
  // { path: 'stock/create', component: StockCreateComponent },
  // { path: 'stock/edit/:id', component: StockEditComponent },
  // { path: 'shop', component: ShopComponent },
  // { path: 'transaction', component: TransactionComponent },
  // { path: 'transaction/detail/:id', component: TransactionDetailComponent },
  //{ path: 'report', component: ReportComponent },
  //{ path: '**', redirectTo: '/login' },
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    LoginComponent,
    MainComponent,
    DailyLineMonitoringComponent,
    DetailByProcessComponent,
    HistoryComponent,
    StockComponent,
    MainMononitorComponent,
    MasterSetupComponent
  ],

  
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
