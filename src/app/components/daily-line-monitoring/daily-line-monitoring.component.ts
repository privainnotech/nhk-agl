import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-daily-line-monitoring',
  templateUrl: './daily-line-monitoring.component.html',
  styleUrls: ['./daily-line-monitoring.component.css']
})
export class DailyLineMonitoringComponent implements OnInit {
  selectedRow = 0;
  index_setup:any[] = [1,2,3,4,5,6,7,8,9];
  setup:any[] = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
  // setup = {
  //   number:"1",
  //   data:"eiei"
  // }
  //setup:any[] = [1,2,3];
  //index:number = 3;

  constructor() { }

  ngOnInit() {
    console.log(this.setup)
  }

  
  selectRow(id){
    this.selectedRow = id;
    console.log("selectedRow = " + this.selectedRow);
  }
  
}
